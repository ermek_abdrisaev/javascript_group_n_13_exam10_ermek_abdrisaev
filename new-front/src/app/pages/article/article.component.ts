import { Component, OnDestroy, OnInit } from '@angular/core';
import { Article } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.sass']
})
export class ArticleComponent implements OnInit {
  articles!: Article[];
  article: Article | null = null;
  constructor( private articlesService: ArticlesService, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

  getArti(id: string){
  this.articles.forEach(item =>{
    if(id === item.id){
      this.article = item;
    }
  });
  this.articlesService.getArticle(id);
  }

}
