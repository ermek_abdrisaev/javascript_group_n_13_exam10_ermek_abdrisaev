import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ArticlesService } from '../../services/articles.service';
import { ArticleData } from '../../models/article.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-articles',
  templateUrl: './new-articles.component.html',
  styleUrls: ['./new-articles.component.sass']
})
export class NewArticlesComponent implements OnInit {
  @ViewChild('f') form!: NgForm;
  constructor(
    private articlesService: ArticlesService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(){
    const articleData: ArticleData = this.form.value;
    this.articlesService.createArticle(articleData).subscribe( () =>{
      void this.router.navigate(['/']);
    });
    this.form.resetForm();

  }
}
