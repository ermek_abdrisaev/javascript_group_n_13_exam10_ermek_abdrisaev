import { Component, OnInit } from '@angular/core';
import { Article } from '../../models/article.model';
import { ArticlesService } from '../../services/articles.service';

@Component({
  selector: 'app-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.sass']
})
export class ArticlesComponent implements OnInit {
  articles!: Article[];
  article: Article | null = null;
  constructor(private articleService: ArticlesService) { }

  ngOnInit(): void {
    this.articleService.getArticles().subscribe(articles =>{
      this.articles = articles.reverse();
    });
  }

  getArti(id: string){
    this.articles.forEach(item =>{
      if(id === item.id){
        this.article = item;
      }
      console.log(item);
    });
    this.articleService.getArticle(id);
  }

  // onRemoveArt(){
  //   this.articleService.deleteArticle(this.article.id).subscribe( () =>{
  //     this.articleService.getArticles();
  //   })
  // }

}
