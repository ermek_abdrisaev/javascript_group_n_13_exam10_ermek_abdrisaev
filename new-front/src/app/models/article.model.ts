export class Article {
  constructor(
    public id: string,
    public title: string,
    public description: string,
    public image: string,
    public datetime: string,
    public author: string,
  ){}
}

export interface ArticleData {
  [key: string]: any;
  title: string;
  description: string;
  author: string;
  datetime: string;
  image: File | null;
}
