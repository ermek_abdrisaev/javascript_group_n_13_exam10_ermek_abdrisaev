import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'anon'
})
export class AnonPipe implements PipeTransform {

  transform(value: string): string {
    if(!value){
      return 'Anonymous';
    } else {
      return value;
    }
  }

}
