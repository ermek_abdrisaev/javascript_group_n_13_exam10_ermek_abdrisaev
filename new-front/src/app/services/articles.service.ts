import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Article, ArticleData } from '../models/article.model';
import { environment } from '../../environments/environment';
import { map, tap } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class ArticlesService {
  private articles: Article[] =[];
  arrMessagesChange = new Subject<Article[]>();
  articleRemoving = new Subject<boolean>();
  constructor(private http: HttpClient, private router: Router) { }

  getArticles(){
    return this.http.get<Article[]>(environment.apiUrl + '/articles').pipe(
      map(response =>{
        return response.map(articleData =>{
          return new Article(
            articleData.id,
            articleData.title,
            articleData.description,
            articleData.image,
            articleData.datetime,
            articleData.author,
          );
        });
      })
    )
  }

  getArticle(id: string){
    return this.http.get<Article | null>(environment.apiUrl + `/articles/${id}`).pipe(
      map(result =>{
        if(!result){
          return null;
        }
        return new Article(id, result.title, result.description, result.image, result.datetime, result.author);
      })
    );
  }

  createArticle(articleData: ArticleData){
    const formData = new FormData();
    Object.keys(articleData).forEach(key =>{
      if(articleData[key] !== null){
        formData.append(key, articleData[key]);
      }
    });
    return this.http.post(environment.apiUrl + '/articles', formData);
  }

  deleteArticle(id: string){
    return this.http.delete(environment.apiUrl + `/articles/${id}`).subscribe( ()=>{
      void this.router.navigate(['/articles']);
    })

  }
}
