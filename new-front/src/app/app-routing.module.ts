import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ArticlesComponent } from './pages/articles/articles.component';
import { NewArticlesComponent } from './pages/new-articles/new-articles.component';
import { ArticleComponent } from './pages/article/article.component';

const routes: Routes = [
  {path: '', component: ArticlesComponent},
  {path: 'new-articles', component: NewArticlesComponent},
  {path: 'article/:id', component: ArticleComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
