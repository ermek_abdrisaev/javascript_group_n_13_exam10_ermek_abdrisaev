const express = require('express');
const db = require('../fileDb');
const router = express.Router();
const pathUrl = './db.json';

router.get('/', async (req, res) =>{
  await db.init(pathUrl);
  const comments = db.getItems();
  return res.send(comments);
});

router.get('/:id', async (req, res) =>{
  await db.init(pathUrl);
  const comment = db.getItem(req.params.id);
  if(!comment){
    return res.status(404).send({mesage: "Not found"});
  }
  return res.send(comment);
});

router.post('/', async (req, res, next) =>{
  try{
    await db.init(pathUrl);
    const comment = {
      title: req.body.title,
      comment: req.body.comment,
    }
    await db.addItem(comment);
    return res.send({message: 'Created comment', id: comment.id});
  } catch(e){
    next(e);
  }

});

router.delete('/:id', async(req, res) =>{
  await db.init(pathUrl);
  const id = db.getItem(req.params.id);
  return res.send('Chosen comment will be deleted from here');
});

module.exports = router;