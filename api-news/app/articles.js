const express = require('express');
const multer = require('multer');
const path = require('path');
const {nanoid} = require('nanoid');
const config = require('../config');
const db = require('../fileDb');
const pathUrl = './db.json';

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) =>{
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) =>{
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) =>{
  await db.init(pathUrl);
  const articles = db.getItems();
  return res.send(articles);
});

router.get('/:id', async (req, res) =>{
  await db.init(pathUrl);
  const article = db.getItem(req.params.id);
  if(!article){
    return res.status(404).send({message: "No such article"});
  }
  return res.send(article);
});

router.post('/', upload.single('image'), async (req, res, next) =>{
  try{
    await db.init(pathUrl);
    if(!req.body.title || !req.body.description){
      return res.status(404).send({message: 'Title and description have to be filled'});
    }
    const article = {
      title: req.body.title,
      description: req.body.description,
      author: req.body.author,
      image: null,
      datetime: new Date()
    }
    if(req.file){
      article.image = req.file.filename;
    }
    await db.addItem(article);
    return res.send({message: 'Created article', id: article.id});
  }catch(e){
    next(e)
  }
});

router.delete('/article/:id', async (req, res) =>{
  let id = db.getItem(req.params.id);
  if(!id){
    return res.status(404).send({message: "Cant find such article!!!"});
  }
  return res.send('Chosen article deleted!');
});

module.exports = router;