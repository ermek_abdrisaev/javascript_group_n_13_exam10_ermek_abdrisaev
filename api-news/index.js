const express = require('express');
const cors = require('cors');
const db = require('./fileDb');
const articles = require('./app/articles');
const comments = require('./app/comments');
const app = express();
const port = 8080;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/articles', articles);
app.use('/comments', comments);

const run = async () =>{
  await db.init();
    app.listen(port, () =>{
      console.log(`Server started on ${port} port`);
    });
}
 run().catch(e => console.error(e));


